'use strict';

var fs = require('fs');
var path = require('path');
var parse = require('csv-parse');
var nodemailer = require('nodemailer');
var handlebars = require('express-handlebars');
var hbs = require('nodemailer-express-handlebars');

// create reusable transporter object using the default SMTP transport
var smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'airwater4@gmail.com',
        pass: 'TYPE YOUR PASSWORD'
    }
};

var transporter = nodemailer.createTransport(smtpConfig);
var viewEngine = handlebars.create({});
transporter.use('compile', hbs({
            viewEngine: viewEngine,
            viewPath: path.resolve(__dirname, './views')
        }));

var parser = parse({delimiter: ';'}, (err, data) => {
    if (err) {
        return console.log(err);
    }
    data.forEach(el => {
        var mail = {
            from: 'airwater4@gmail.com',
            to: el[2],
            subject: 'Test',
            template: 'email',
            context: {
                name: el[0],
                company: el[1]
            }
        };
        transporter.sendMail(mail, (err, info) => {
            if (err) {
                return console.log(err);
            }
            console.log('Message sent: ' + info.response);
        });
    });
});

fs.createReadStream(__dirname + '/clients.csv').pipe(parser);